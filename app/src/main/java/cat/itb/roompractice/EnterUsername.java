package cat.itb.roompractice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class EnterUsername extends AppCompatActivity implements View.OnClickListener
{
    private EditText editTextUser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_username);

        editTextUser = findViewById(R.id.enterUsernameEdit);
        findViewById(R.id.enterUsernameButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(editTextUser.getText().toString().isEmpty())
            Toast.makeText(this, "Please Write a Username to be able to start!", Toast.LENGTH_SHORT).show();
        else
        {
            Intent intent = new Intent(EnterUsername.this, Game.class);
            intent.putExtra("user", editTextUser.getText().toString());
            startActivity(intent);
            finish();
        }
    }

}