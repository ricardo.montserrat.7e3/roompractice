package cat.itb.roompractice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;

public class ScoresActivity extends AppCompatActivity
{
    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        findViewById(R.id.goBackButton).setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { finish(); }});

        adapter = new MyAdapter(MainActivity.databaseRepo.getAllScores(), R.layout.scores_view_item);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        registerForContextMenu(recyclerView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu_scores, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == R.id.deleteAllOption)
        {
            MainActivity.databaseRepo.deleteAllScores();
            adapter.notifyDataSetChanged();
            return true;
        }
        return super.onContextItemSelected(item);
    }
}