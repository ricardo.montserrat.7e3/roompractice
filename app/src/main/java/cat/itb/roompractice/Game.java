package cat.itb.roompractice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import cat.itb.roompractice.Database.Question;
import cat.itb.roompractice.Database.UserScore;

public class Game extends AppCompatActivity implements EditText.OnKeyListener
{
    private EditText editTextAnswer;
    private TextView question, questionsLeft, timerText;

    private List<Question> questionsSelected;
    private int pointer = 0;

    private int maxSecondsTimer = 10;
    private CountDownTimer actualTimer;

    private int userScore = 0;

    private void loadQuestion()
    {
        if (questionsSelected.size() > 0)
        {
            Question currentQuestion = questionsSelected.get(pointer);
            question.setText(currentQuestion.getStatement());
            editTextAnswer.setText("");
            questionsLeft.setText(((pointer + 1) + " out of " + questionsSelected.size()));
            timerText.setText(String.valueOf(maxSecondsTimer));
        }
    }

    private void startTimer()
    {
        if (actualTimer != null) actualTimer.cancel();
        actualTimer = new CountDownTimer(maxSecondsTimer * 1000, 1000)
        {
            @Override
            public void onTick(long millisUntilFinished) { timerText.setText(String.valueOf(millisUntilFinished / 1000)); }

            @Override
            public void onFinish()
            {
                loadNextQuestion();
                startTimer();
            }
        };
        actualTimer.start();
    }

    private void loadNextQuestion()
    {
        if (pointer != questionsSelected.size() - 1)
        {
            pointer = (pointer + 1) % questionsSelected.size();
            loadQuestion();
        }
        else
        {
            Bundle values = getIntent().getExtras();
            if (values != null) MainActivity.databaseRepo.insert(new UserScore(0, userScore, values.getString("user")));
            Intent intent = new Intent(Game.this, FinalScreen.class);
            intent.putExtra("score", userScore);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        editTextAnswer = findViewById(R.id.answerEditText);
        editTextAnswer.setOnKeyListener(this);

        question = findViewById(R.id.questionTextView);
        questionsLeft = findViewById(R.id.questionsLeftTextView);
        timerText = findViewById(R.id.timerTextView);

        questionsSelected = MainActivity.databaseRepo.getRandoms(5);

        loadQuestion();
        startTimer();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_ENTER)
        {
            if (editTextAnswer.getText().toString().equalsIgnoreCase(questionsSelected.get(pointer).getAnswer())) userScore++;
            loadNextQuestion();
            startTimer();
            if(pointer != questionsSelected.size() - 1) actualTimer.cancel();
            return true;
        }
        return false;
    }
}