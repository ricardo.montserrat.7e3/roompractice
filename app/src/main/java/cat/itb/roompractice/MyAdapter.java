package cat.itb.roompractice;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.roompractice.Database.UserScore;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>
{
    private List<UserScore> users;
    private int layout;

    public MyAdapter(List<UserScore> users, int layout)
    {
        this.users = users;
        this.layout = layout;
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) { return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false)); }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder holder, int position)
    {
        UserScore currentUser = users.get(position);
        holder.userTextView.setText(currentUser.getUsername());
        holder.userScoreTextView.setText(String.valueOf(currentUser.getScore()));
    }

    @Override
    public int getItemCount() { return users.size(); }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView userTextView, userScoreTextView;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            userTextView = itemView.findViewById(R.id.userNameTextView);
            userScoreTextView = itemView.findViewById(R.id.userScoreTextView);
        }
    }
}
