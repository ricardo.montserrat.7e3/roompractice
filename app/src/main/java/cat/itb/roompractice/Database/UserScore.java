package cat.itb.roompractice.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Scores")
public class UserScore
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_scores")
    int id;
    int score;
    String username;

    public UserScore(int id, int score, String username)
    {
        this.id = id;
        this.score = score;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public String getUsername() {
        return username;
    }
}
