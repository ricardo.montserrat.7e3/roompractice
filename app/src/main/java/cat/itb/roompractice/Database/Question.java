package cat.itb.roompractice.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "Questions")
public class Question
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_question")
    int id;
    String statement, answer;

    public Question(int id, String statement, String answer) {
        this.id = id;
        this.statement = statement;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatement() {
        return statement;
    }

    public String getAnswer() {
        return answer;
    }
}
