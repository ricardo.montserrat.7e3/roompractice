package cat.itb.roompractice.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UserScoresDao
{

    @Query("SELECT * FROM Scores ORDER BY score")
    List<UserScore> getAll();

    @Insert
    void insert(UserScore s);

    @Delete
    void delete(UserScore s);

    @Update
    void update(UserScore s);

    @Query("SELECT * FROM Scores WHERE id_scores = :idUserScore")
    UserScore findById(int idUserScore);

    @Query("DELETE FROM Scores")
    void deleteAll();
}
