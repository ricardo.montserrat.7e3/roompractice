package cat.itb.roompractice.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
@Dao
public interface QuestionDao
{

    @Insert
    void insert(Question s);

    @Query("SELECT * FROM Questions ORDER BY RANDOM() LIMIT :total")
    List<Question> getRandom(int total);

    @Query("SELECT COUNT(*) FROM Questions")
    int getQuestionsNumber();
}
