package cat.itb.roompractice.Database;

import java.util.List;

public class Repository
{
    QuestionDao questionDao;
    UserScoresDao scoresDao;

    public Repository(QuestionDao questionDao, UserScoresDao scoresDao)
    {
        this.questionDao = questionDao;
        this.scoresDao = scoresDao;
    }

    public void insert(Question s)
    {
        this.questionDao.insert(s);
    }

    public List<Question> getRandoms(int total) { return this.questionDao.getRandom(total); }

    public int getQuestionsNumber() { return this.questionDao.getQuestionsNumber(); }


    public List<UserScore> getAllScores() { return this.scoresDao.getAll(); }

    public void insert(UserScore s)
    {
        this.scoresDao.insert(s);
    }

    public void deleteAllScores() { this.scoresDao.deleteAll(); }
}
