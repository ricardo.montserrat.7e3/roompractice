package cat.itb.roompractice.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {UserScore.class, Question.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase
{
    public static AppDatabase instance;

    public abstract UserScoresDao scoresDao();
    public abstract QuestionDao questionDao();

    public static AppDatabase getInstance(Context context) { return instance != null? instance :
            Room.databaseBuilder(context, AppDatabase.class, "QuestionsGame.db").allowMainThreadQueries().fallbackToDestructiveMigration().build(); }
}
