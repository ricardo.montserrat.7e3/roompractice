package cat.itb.roompractice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import cat.itb.roompractice.Database.AppDatabase;
import cat.itb.roompractice.Database.Question;
import cat.itb.roompractice.Database.Repository;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    public static Repository databaseRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppDatabase tempData = AppDatabase.getInstance(this.getApplicationContext());
        databaseRepo = new Repository(tempData.questionDao(), tempData.scoresDao());

        findViewById(R.id.playButton).setOnClickListener(this);
        findViewById(R.id.seeStatsButton).setOnClickListener(this);

        if (databaseRepo.getQuestionsNumber() == 0)
        {
            databaseRepo.insert(new Question(0, "2+2 = ?", "4"));
            databaseRepo.insert(new Question(0, "4+6 = ?", "10"));
            databaseRepo.insert(new Question(0, "6+7 = ?", "13"));
            databaseRepo.insert(new Question(0, "10+20 = ?", "30"));
            databaseRepo.insert(new Question(0, "15+60 = ?", "75"));

            databaseRepo.insert(new Question(0, "15-9 = ?", "6"));
            databaseRepo.insert(new Question(0, "5-8 = ?", "-3"));
            databaseRepo.insert(new Question(0, "17-4 = ?", "13"));
            databaseRepo.insert(new Question(0, "11-15 = ?", "-4"));
            databaseRepo.insert(new Question(0, "19-7 = ?", "12"));

            databaseRepo.insert(new Question(0, "60/3 = ?", "20"));
            databaseRepo.insert(new Question(0, "70/2 = ?", "35"));
            databaseRepo.insert(new Question(0, "5/5 = ?", "1"));
            databaseRepo.insert(new Question(0, "6/3 = ?", "2"));
            databaseRepo.insert(new Question(0, "18/6 = ?", "3"));

            databaseRepo.insert(new Question(0, "19*2 = ?", "38"));
            databaseRepo.insert(new Question(0, "8*8 = ?", "64"));
            databaseRepo.insert(new Question(0, "16*3 = ?", "48"));
            databaseRepo.insert(new Question(0, "4*14 = ?", "56"));
            databaseRepo.insert(new Question(0, "2*4 = ?", "8"));
        }
    }

    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.playButton: intent = new Intent(MainActivity.this, EnterUsername.class); break;
            case R.id.seeStatsButton: intent = new Intent(MainActivity.this, ScoresActivity.class);break;
            default: intent = new Intent(); break;
        }
        startActivity(intent);
    }
}