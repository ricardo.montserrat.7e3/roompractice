package cat.itb.roompractice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FinalScreen extends AppCompatActivity implements View.OnClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_screen);

        findViewById(R.id.goBackButton).setOnClickListener(this);

        Bundle values = getIntent().getExtras();
        if (values != null) ((TextView) findViewById(R.id.userScoreTextView)).setText(("Your Final Score is: " + values.getInt("score")));
    }

    @Override
    public void onClick(View v) { finish(); }
}